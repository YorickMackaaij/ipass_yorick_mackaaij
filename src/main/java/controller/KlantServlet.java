package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Klant;
import domain.service.ServiceProvider;

public class KlantServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		Klant k = ServiceProvider.getAutoVerhuurService().getKlant(1);
		req.setAttribute("klant", k);
		req.getRequestDispatcher("/klant/mijn_gegevens.jsp").forward(req, resp);
	}
}
