package services;

import java.util.List;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import domain.Auto;
import domain.Verhuur;
import domain.service.ServiceProvider;

@Path("/klant")
public class KlantResource {
	
	@GET
	@Path("/reserveringen/{id}/{from}/{limit}")
	@Produces("application/json")
	public String getReserveringen(@PathParam("id") int id,
									@PathParam("from") int from,
									@PathParam("limit") int limit){
		JsonArrayBuilder jab = Json.createArrayBuilder();
		
		List<Verhuur> reserveringen = ServiceProvider.getAutoVerhuurService().getReserveringen(id, from, limit);
	
		for(Verhuur v : reserveringen){
			JsonObjectBuilder job = Json.createObjectBuilder();
			job.add("autoid", v.getAutoid());
			job.add("startdatum", v.getStartdatum().toString());
			job.add("einddatum", v.getEinddatum().toString());
			
			Auto a = ServiceProvider.getAutoVerhuurService().getAuto(v.getAutoid());
			job.add("merk", a.getMerk());
			job.add("model", a.getModel());
			job.add("type", a.getType());
			job.add("prijs", a.getPrijs());
			
			jab.add(job.build());
		}
		return jab.build().toString();
	}
	
}