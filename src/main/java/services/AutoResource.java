package services;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.FormParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import domain.Auto;
import domain.Verhuur;
import domain.service.AutoVerhuurService;
import domain.service.ServiceProvider;

@Path("/autos")
public class AutoResource {
	
	@POST
	@Path("/update")
	@Produces("application/json")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String updateAuto(@FormParam("id") int id,
							@FormParam("merk") String merk,
							@FormParam("model") String model,
							@FormParam("type") String type,
							@FormParam("prijs") Double prijs,
							@FormParam("brandstof") String brandstof,
							@FormParam("verhuurbaar") boolean verhuurbaar){
		JsonObjectBuilder job = Json.createObjectBuilder();
		
		JsonArrayBuilder errorbuilder = validateInput(merk, model, type, prijs, brandstof);
		
		JsonArray errors = errorbuilder.build();
		job.add("errors", errors);
		if(errors.size() == 0)
			ServiceProvider.getAutoVerhuurService().updateAuto(new Auto(id, merk, model, type, prijs, Integer.parseInt(brandstof), verhuurbaar));
		String rtn = job.build().toString();
		return rtn;
	}
	
	@POST
	@Path("/new")
	@Produces("application/json")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String newAuto(@FormParam("merk") String merk,
							@FormParam("model") String model,
							@FormParam("type") String type,
							@FormParam("prijs") Double prijs,
							@FormParam("brandstof") String brandstof,
							@FormParam("verhuurbaar") boolean verhuurbaar){
		JsonObjectBuilder job = Json.createObjectBuilder();
		
		JsonArrayBuilder errorbuilder = validateInput(merk, model, type, prijs, brandstof);
		
		JsonArray errors = errorbuilder.build();
		job.add("errors", errors);
		if(errors.size() == 0)
			ServiceProvider.getAutoVerhuurService().addAuto(new Auto(merk, model, type, prijs, Integer.parseInt(brandstof), verhuurbaar));
		String rtn = job.build().toString();
		return rtn;
	}
	
	@GET
	@Path("/all")
	@Produces("application/json")
	public String getAllAutos(){
		JsonArray autos = buildJsonAutoArray(ServiceProvider.getAutoVerhuurService().getAllAutos(0, 100));
		return autos.toString();
	}
	
	@GET
	@Path("/delete/{id}")
	public void deleteAuto(@PathParam("id") int id){
		ServiceProvider.getAutoVerhuurService().deleteAuto(id);
	}
	
	@GET
	@Path("/all/{from}/{limit}")
	@Produces("application/json")
	public String getAllAutos(@PathParam("from") int from, @PathParam("limit") int limit){		
		JsonArray autos = buildJsonAutoArray(ServiceProvider.getAutoVerhuurService().getAllAutos(from, limit));
		return autos.toString();
	}
	
	@GET
	@Path("/verhuurbaar/{from}/{limit}")
	@Produces("application/json")
	public String getAllVerhuurbareAutos(@PathParam("from") int from, @PathParam("limit") int limit){
		JsonObjectBuilder job = Json.createObjectBuilder();
		
		JsonArray autos = buildJsonAutoArray(ServiceProvider.getAutoVerhuurService().getAllVerhuurbareAutos(from, limit));
		
		List<Verhuur> verhuurdatums = ServiceProvider.getAutoVerhuurService().getVerhuurDatums();
		JsonArrayBuilder datums = Json.createArrayBuilder();
		for(Verhuur v : verhuurdatums){
			JsonObjectBuilder job2 = Json.createObjectBuilder();
			job2.add("autoid", v.getAutoid());
			job2.add("klantid", v.getKlantid());
			job2.add("startdatum", v.getStartdatum().toString());
			job2.add("einddatum", v.getEinddatum().toString());
			
			datums.add(job2.build());
		}
		
		job.add("datums", datums.build());
		job.add("autos", autos);
		return job.build().toString();
	}
	
	@GET
	@Path("/total")
	@Produces("application/json")
	public String getTotalAutos(){
		JsonObjectBuilder autos = Json.createObjectBuilder().add("total", ServiceProvider.getAutoVerhuurService().getTotalAutos());
		return autos.build().toString();
	}
	
	@GET
	@Path("/total/verhuurbaar")
	@Produces("application/json")
	public String getTotalVerhuurbareAutos(){
		JsonObjectBuilder autos = Json.createObjectBuilder().add("total", ServiceProvider.getAutoVerhuurService().getTotalVerhuurbareAutos());
		return autos.build().toString();
	}
	
	@POST
	@Path("/huur")
	@Produces("application/json")
	public String huurAuto(@FormParam("klantid") int klantid,
							@FormParam("autoid") int autoid,
							@FormParam("vanaf") String vanaf,
							@FormParam("tot") String tot){
		boolean error = false;
		DateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.GERMAN);
		try {
			Date vanaf_date = new java.sql.Date(format.parse(vanaf).getTime());
			Date tot_date = new java.sql.Date(format.parse(tot).getTime());
			
			Calendar cal = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			cal.setTime(vanaf_date);
			cal2.setTime(tot_date);
			
			// check of de datums kloppen..
			if(cal.get(Calendar.YEAR) != cal2.get(Calendar.YEAR)){
				error = true;
			}else if(cal.getTimeInMillis()+86400000 <  System.currentTimeMillis()){
				error = true;
			}else if(cal2.getTimeInMillis() < cal.getTimeInMillis()){
				error = true;
			}
			
			// check of de datum beschikbaar is ( nog niet verhuurd )
			List<Verhuur> verhuurdatums = ServiceProvider.getAutoVerhuurService().getVerhuurDatums(autoid);
			for(Verhuur v : verhuurdatums){
				if(vanaf_date.getTime() <= v.getEinddatum().getTime() && tot_date.getTime() >= v.getStartdatum().getTime()){
					error = true;
					break;
				}
			}
			
			if(!error)
				if(!ServiceProvider.getAutoVerhuurService().huurAuto(autoid, klantid, vanaf_date, tot_date))
					error = true;
		} catch (ParseException e) {
			error = true;
		}
		
		JsonObjectBuilder job = Json.createObjectBuilder();
		if(error)
			job.add("message", "false");
		else
			job.add("message", "true");
		
		return job.build().toString();
	}
	
	private JsonArray buildJsonAutoArray(List<Auto> autos) {
		JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
		
		for (Auto a : autos) {
			JsonObjectBuilder job = Json.createObjectBuilder();
			
			job.add("id", a.getId());
			job.add("merk", a.getMerk());
			job.add("model", a.getModel());
			job.add("type", a.getType());
			job.add("prijs", a.getPrijs());
			job.add("brandstof", a.getBrandstof());
			job.add("verhuurbaar", a.isVerhuurbaar());
			
			jsonArrayBuilder.add(job);
		}
		return jsonArrayBuilder.build();
	}
	
	private JsonArrayBuilder validateInput(String merk, String model, String type, Double prijs, String brandstof){
		JsonArrayBuilder errorbuilder = Json.createArrayBuilder();
		
		// merk
		if(merk.isEmpty())
			errorbuilder.add("Merk moet ingevuld zijn.");
		else if(merk.length() > 50)
			errorbuilder.add("Merk mag niet meer dan 50 characters bevatten.");
		// model
		if(model.isEmpty())
			errorbuilder.add("Model moet ingevuld zijn.");
		else if(model.length() > 50)
			errorbuilder.add("Model mag niet meer dan 50 characters bevatten.");
		// type
		if(type.isEmpty())
			errorbuilder.add("Type moet ingevuld zijn.");
		else if(type.length() > 50)
			errorbuilder.add("Type mag niet meer dan 50 characters bevatten.");
		// prijs
		if(String.valueOf(prijs).length() > 9)
			errorbuilder.add("Prijs is te groot (niet meer dan 9 characters).");
		else if(null == prijs || prijs <= 0)
			errorbuilder.add("Prijs mag niet negatief of nul zijn.");
		// brandstof
		if(String.valueOf(brandstof).length() > 3)
			errorbuilder.add("Brandstof is te groot (niet meer dan 3 characters).");
		else if(!brandstof.matches("[-+]?\\d*\\.?\\d+"))
			errorbuilder.add("Brandstof moet een getal zijn.");
		return errorbuilder;
	}
}