package persistence;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Auto;
import domain.Klant;
import domain.Verhuur;

public class KlantDAO extends BaseDAO{
	public Klant getKlant(int klantid){
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("SELECT voornaam, tussenvoegsel, achternaam, adres, postcode FROM klant WHERE klantid = ?");
			ps.setInt(1, klantid);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				String voornaam = rs.getString("voornaam");
				String tussenvoegsel = rs.getString("tussenvoegsel");
				String achternaam = rs.getString("achternaam");
				String adres = rs.getString("adres");
				String postcode = rs.getString("postcode");
				if(tussenvoegsel == null)
					tussenvoegsel = "";
				Klant k = new Klant(klantid, voornaam, tussenvoegsel, achternaam, adres, postcode);
				return k;
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Verhuur> getReserveringen(int klantid, int from, int limit){
		List<Verhuur> verhuur = new ArrayList<Verhuur>();
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("SELECT autoid, startdatum, einddatum from verhuur where klantid = ? LIMIT ?, ?");
			ps.setInt(1, klantid);
			ps.setInt(2, from);
			ps.setInt(3, limit);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				int autoid = rs.getInt("autoid");
				Date startdatum = rs.getDate("startdatum");
				Date einddatum = rs.getDate("einddatum");
				Verhuur v = new Verhuur(autoid, klantid, startdatum, einddatum);
				verhuur.add(v);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return verhuur;
	}
}