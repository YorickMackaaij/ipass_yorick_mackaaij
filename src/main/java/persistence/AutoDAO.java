package persistence;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Auto;
import domain.Verhuur;

public class AutoDAO extends BaseDAO{
	
	public List<Auto> getAllVerhuurbareAutos(int from, int limit){
		List<Auto> list = new ArrayList<Auto>();
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("SELECT id, merk, model, type, prijs, brandstof, verhuurbaar FROM auto WHERE verhuurbaar = true LIMIT ?, ?");
			ps.setInt(1, from);
			ps.setInt(2, limit);
			ResultSet rs = ps.executeQuery();
			
			
			while(rs.next()){
				int id = rs.getInt("id");
				String merk = rs.getString("merk");
				String model = rs.getString("model");
				String type = rs.getString("type");
				Double prijs = rs.getDouble("prijs");
				int brandstof = rs.getInt("brandstof");
				boolean verhuurbaar = rs.getBoolean("verhuurbaar");
				
				list.add(new Auto(id, merk, model, type, prijs, brandstof, verhuurbaar));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return list;
	}
	
	public Auto getAuto(int id){
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("SELECT merk, model, type, prijs, brandstof, verhuurbaar FROM auto WHERE id = ?");
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				String merk = rs.getString("merk");
				String model = rs.getString("model");
				String type = rs.getString("type");
				Double prijs = rs.getDouble("prijs");
				int brandstof = rs.getInt("brandstof");
				boolean verhuurbaar = rs.getBoolean("verhuurbaar"); 
				
				return new Auto(id, merk, model, type, prijs, brandstof, verhuurbaar);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Auto> getAllAutos(int from, int limit){
		List<Auto> list = new ArrayList<Auto>();
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("SELECT id, merk, model, type, prijs, brandstof, verhuurbaar FROM auto LIMIT ?, ?");
			ps.setInt(1, from);
			ps.setInt(2, limit);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				int id = rs.getInt("id");
				String merk = rs.getString("merk");
				String model = rs.getString("model");
				String type = rs.getString("type");
				Double prijs = rs.getDouble("prijs");
				int brandstof = rs.getInt("brandstof");
				boolean verhuurbaar = rs.getBoolean("verhuurbaar");
				
				list.add(new Auto(id, merk, model, type, prijs, brandstof, verhuurbaar));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return list;
	}
	
	public void saveAuto(Auto auto){
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("INSERT INTO auto(merk, model, type, prijs, brandstof, verhuurbaar) VALUES(?, ?, ?, ?, ?, ?)");
			ps.setString(1, auto.getMerk());
			ps.setString(2, auto.getModel());
			ps.setString(3, auto.getType());
			ps.setDouble(4, auto.getPrijs());
			ps.setInt(5, auto.getBrandstof());
			ps.setBoolean(6, auto.isVerhuurbaar());
			ps.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void deleteAuto(int id){
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("DELETE FROM verhuur WHERE autoid = ?");
			ps.setInt(1, id);
			ps.executeUpdate();
			
			ps = con.prepareStatement("DELETE FROM auto WHERE id = ?");
			ps.setInt(1, id);
			ps.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public boolean huurAuto(int autoid, int klantid, Date vanaf, Date tot){
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("INSERT INTO verhuur (autoid, klantid, startdatum, einddatum)"
													 + " VALUES(?,?,?,?)");
			ps.setInt(1, autoid);
			ps.setInt(2, klantid);
			ps.setDate(3, vanaf);
			ps.setDate(4, tot);
			ps.executeUpdate();
			return true;
		}catch(SQLException e){
			e.printStackTrace();
			return false;
		}
	}
	
	public void updateAuto(Auto auto){
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("UPDATE auto SET "
					+ "merk = ?,"
					+ "model = ?,"
					+ "type = ?,"
					+ "prijs = ?,"
					+ "brandstof = ?,"
					+ "verhuurbaar = ? "
					+ "WHERE id = ?");
			ps.setString(1, auto.getMerk());
			ps.setString(2, auto.getModel());
			ps.setString(3, auto.getType());
			ps.setDouble(4, auto.getPrijs());
			ps.setInt(5, auto.getBrandstof());
			ps.setBoolean(6, auto.isVerhuurbaar());
			ps.setInt(7, auto.getId());
			ps.executeUpdate();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public int getTotalAutos(){
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("SELECT COUNT(*) as total FROM auto");
			ResultSet rs = ps.executeQuery();
			if(rs.next())
				return rs.getInt("total");
		}catch(SQLException e){
			e.printStackTrace();
		}
		return 0;
	}
	
	public int getTotalVerhuurbareAutos(){
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("SELECT COUNT(*) as total FROM auto WHERE auto.verhuurbaar = true");
			ResultSet rs = ps.executeQuery();
			if(rs.next())
				return rs.getInt("total");
		}catch(SQLException e){
			e.printStackTrace();
		}
		return 0;
	}
	
	public List<Verhuur> getVerhuurDatums(int autoid){
		List<Verhuur> verhuur = new ArrayList<Verhuur>();
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("select klantid, autoid, startdatum, einddatum FROM verhuur JOIN auto ON auto.id = verhuur.autoid WHERE auto.verhuurbaar = true AND auto.id = ?");
			ps.setInt(1, autoid);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				verhuur.add(new Verhuur(rs.getInt("autoid"), rs.getInt("klantid"), rs.getDate("startdatum"), rs.getDate("einddatum")));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return verhuur;
	}
	
	public List<Verhuur> getVerhuurDatums(){
		List<Verhuur> verhuur = new ArrayList<Verhuur>();
		try(Connection con = super.getConnection()){
			PreparedStatement ps = con.prepareStatement("select klantid, autoid, startdatum, einddatum FROM verhuur JOIN auto ON auto.id = verhuur.autoid WHERE auto.verhuurbaar = true");
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()){
				verhuur.add(new Verhuur(rs.getInt("autoid"), rs.getInt("klantid"), rs.getDate("startdatum"), rs.getDate("einddatum")));
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return verhuur;
	}
}
