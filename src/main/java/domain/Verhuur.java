package domain;

import java.sql.Date;

public class Verhuur {
	private int autoid, klantid;
	private Date startdatum, einddatum;
	
	public Verhuur(int autoid, int klantid, Date startdatum, Date einddatum){
		this.autoid = autoid;
		this.klantid = klantid;
		this.startdatum = startdatum;
		this.einddatum = einddatum;
	}

	public int getAutoid() {
		return autoid;
	}

	public int getKlantid() {
		return klantid;
	}

	public Date getStartdatum() {
		return startdatum;
	}

	public void setStartdatum(Date startdatum) {
		this.startdatum = startdatum;
	}

	public Date getEinddatum() {
		return einddatum;
	}

	public void setEinddatum(Date einddatum) {
		this.einddatum = einddatum;
	}
}