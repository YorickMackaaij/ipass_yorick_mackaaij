package domain;

public class Auto {
	private int id; // unique id
	private String merk; // bijvoorbeeld: mercedes
	private String model; // bijvoorbeeld: SLS AMG
	private String type; // bijvoorbeeld: suv, sedan, etc.
	private Double prijs; // de prijs per uur
	private int brandstof; // brandstof in liters
	private boolean verhuurbaar;
	
	public Auto(String merk, String model, String type, Double prijs, int brandstof, boolean verhuurbaar) {
		super();
		this.id = 0;
		this.merk = merk;
		this.model = model;
		this.type = type;
		this.prijs = prijs;
		this.brandstof = brandstof;
		this.verhuurbaar = verhuurbaar;
	}
	
	public Auto(int id, String merk, String model, String type, Double prijs, int brandstof, boolean verhuurbaar) {
		this(merk, model, type, prijs, brandstof, verhuurbaar);
		this.id = id;
	}

	public int getId() {
		return id;
	}
	
	public boolean isVerhuurbaar() {
		return verhuurbaar;
	}

	public void setVerhuurbaar(boolean verhuurbaar) {
		this.verhuurbaar = verhuurbaar;
	}

	public String getMerk() {
		return merk;
	}

	public void setMerk(String merk) {
		this.merk = merk;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Double getPrijs() {
		return prijs;
	}

	public void setPrijs(Double prijs) {
		this.prijs = prijs;
	}

	public int getBrandstof() {
		return brandstof;
	}

	public void setBrandstof(int brandstof) {
		this.brandstof = brandstof;
	}
}