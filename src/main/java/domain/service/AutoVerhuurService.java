package domain.service;

import java.sql.Date;
import java.util.List;

import domain.Auto;
import domain.Klant;
import domain.Verhuur;
import persistence.AutoDAO;
import persistence.KlantDAO;

public class AutoVerhuurService {
	private AutoDAO d = new AutoDAO();
	private KlantDAO k = new KlantDAO();
	
	public void addAuto(Auto auto){
		d.saveAuto(auto);
	}
	
	public List<Auto> getAllAutos(int from, int limit){
		return d.getAllAutos(from, limit);
	}
	
	public List<Auto> getAllVerhuurbareAutos(int from, int limit){
		return d.getAllVerhuurbareAutos(from, limit);
	}
	
	public void deleteAuto(int id){
		d.deleteAuto(id);
	}
	
	public boolean huurAuto(int autoid, int klantid, Date vanaf, Date tot){
		return d.huurAuto(autoid, klantid, vanaf, tot);
	}
	
	public void updateAuto(Auto auto){
		d.updateAuto(auto);
	}
	
	public int getTotalAutos(){
		return d.getTotalAutos();
	}
	
	public Auto getAuto(int autoid){
		return d.getAuto(autoid);
	}
	
	public int getTotalVerhuurbareAutos(){
		return d.getTotalVerhuurbareAutos();
	}
	
	public List<Verhuur> getVerhuurDatums(int autoid){
		return d.getVerhuurDatums(autoid);
	}
	
	public List<Verhuur> getVerhuurDatums(){
		return d.getVerhuurDatums();
	}
	
	public Klant getKlant(int klantid){
		return k.getKlant(klantid);
	}
	
	public List<Verhuur> getReserveringen(int klantid, int from, int limit){
		return k.getReserveringen(klantid, from, limit);
	}
	
}