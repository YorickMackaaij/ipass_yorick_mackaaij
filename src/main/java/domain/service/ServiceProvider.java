package domain.service;

public class ServiceProvider {
	private static AutoVerhuurService service = new AutoVerhuurService();

	public static AutoVerhuurService getAutoVerhuurService() {
		return service;
	}
}