package domain;

public class Klant{
	
	private int id;
	private String voornaam, tussenvoegsels, achternaam, adres, postcode;

	public Klant(int id, String voornaam, String tussenvoegsels, String achternaam, String adres, String postcode) {
		this.id = id;
		this.voornaam = voornaam;
		this.tussenvoegsels = tussenvoegsels;
		this.achternaam = achternaam;
		this.adres = adres;
		this.postcode = postcode;
	}
	
	public Klant(int id, String voornaam, String achternaam, String adres, String postcode) {
		this(id, voornaam, "", achternaam, adres, postcode);
	}

	public int getId(){
		return id;
	}
	
	public String getVoornaam() {
		return voornaam;
	}

	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	public String getTussenvoegsels() {
		return tussenvoegsels;
	}

	public void setTussenvoegsels(String tussenvoegsels) {
		this.tussenvoegsels = tussenvoegsels;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
}