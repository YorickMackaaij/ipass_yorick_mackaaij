var rows_per_page = 5; // hoeveel records er per keer in de table komen
var total_rows; // totaal aantal auto's in de database
var klant_id = 1; // klant die is 'ingelogd'
var current_page = 1;

//maakt de pagination
function initPageNumbers(){
	$('#page-numbers').html('');
	if(null == total_rows){
		$.get('/restservices/klant/reserveringen/'+klant_id+'/'+0+'/'+9999999, function(data){
			total_rows = data.length;
			if(total_rows < rows_per_page){
				return;
			}
			var count = 1;
			for(var x = 0;  x < total_rows; x += rows_per_page)
			{
				if(count == current_page){
					$('#page-numbers').append('<li class=\"active\"><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
				}else{
					$('#page-numbers').append('<li><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
				}
				count++;
			}
		});
	}else{
		var count = 1;
		for(var x = 0;  x < total_rows; x += rows_per_page)
		{
			if(count == current_page){
				$('#page-numbers').append('<li class=\"active\"><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
			}else{
				$('#page-numbers').append('<li><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
			}
			count++;
		}
	}
}

function getPage(page_num){
	$('.centered-box').html('');
	current_page = page_num;
	page_num = (page_num-1)*rows_per_page;
	initPageNumbers();
	
	$.ajax({
		url : '/restservices/klant/reserveringen/'+klant_id+'/'+page_num+'/'+rows_per_page,
		type: "GET",
		success: function(data, textStatus, jqXHR)
		{	
			if(data.length > 0){
				html = '';
				$(data).each(function(index, element){
					html += '<div class="panel panel-default">';
					html += '<div class="panel-heading">';
					html += '<h3 class="panel-title">'+this.merk+' '+this.model+'</h3>';
					html += '</div>';
					html += '<div class="panel-body">';
					html += '<div class="pull-right">';
					html += 'vanaf: '+formatDate(this.startdatum)+'<br>';
					html += 'tot en met: '+formatDate(this.einddatum);
					html += '</div>';
					html += 'Type: '+this.type + '<br>';
					html += 'Prijs per dag: &euro;' + +parseFloat(this.prijs).toFixed(2);
					html += '</div>';
					html += '</div><br>';
				});
				$('.centered-box').prepend(html); 
			}else{
				$('.centered-box').prepend("<h3>U heeft nog geen auto's gereserveerd.</h3>");
			}
		}
	});
}

function formatDate(dateStr){
	var parts = dateStr.split("-");
	return $.datepicker.formatDate('dd-mm-yy', new Date(parts[0], parts[1]-1, parts[2]));
}

if(location.hash.slice(1) == ""){
	getPage(1);
	current_page = 1;
}else if(!isNaN(location.hash.slice(1))){
	getPage(location.hash.slice(1));
	current_page = location.hash.slice(1);
}