var rows_per_page = 5; // hoeveel records er per keer in de table komen
var total_rows; // totaal aantal auto's in de database
var current_page = 1;
var klant_id = 1; // klant die is ingelogd..
var auto_id; // de auto die 'geselecteerd' is
var huur_datums = [];
var blocked_days = []; // een array met datums die gedisabled zijn in jquery datepicker

$("#vanaf").datepicker({ dateFormat: 'dd-mm-yy', maxDate: 365, minDate: 0, beforeShowDay: disableDays });
$("#tot").datepicker({ dateFormat: 'dd-mm-yy', maxDate: 365, minDate: 0, beforeShowDay: disableDays });

// maakt de pagination
function initPageNumbers(){
	$('#page-numbers').html('');
	if(null == total_rows){
		$.get('/restservices/autos/total/verhuurbaar', function(data){
			total_rows = parseInt(data.total);
			
			if(total_rows < rows_per_page){
				return;
			}
			
			var count = 1;
			for(var x = 0;  x < total_rows; x += rows_per_page)
			{
				if(count == current_page){
					$('#page-numbers').append('<li class=\"active\"><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
				}else{
					$('#page-numbers').append('<li><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
				}
				count++;
			}
		});
	}else{
		var count = 1;
		for(var x = 0;  x < total_rows; x += rows_per_page)
		{
			if(count == current_page){
				$('#page-numbers').append('<li class=\"active\"><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
			}else{
				$('#page-numbers').append('<li><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
			}
			count++;
		}
	}
}

function getPage(page_num){
	$('.centered-box').html(''); 
	current_page = page_num;
	page_num = (page_num-1)*rows_per_page;
	
	$.ajax({
		url : '/restservices/autos/verhuurbaar/'+page_num+'/'+rows_per_page,
		type: "GET",
		success: function(data, textStatus, jqXHR)
		{	
			if(data.autos.length > 0){
				html = '';
				huur_datums = data.datums;
				$(data.autos).each(function(index, element){
					html += '<div class="panel panel-default">';
					html += '<div class="panel-heading">';
					html += '<h3 class="panel-title">'+this.merk+' '+this.model+'</h3>';
					html += '</div>';
					html += '<div class="panel-body">';
					html += '<div class="pull-right">';
					html += '<button class="btn btn-primary" onclick="auto_id = '+this.id+'; showModalWindow(); blockDays(); $(\'#vanaf, #tot\').val(\'\'); ">Huren</button>';
					html += '</div>';
					html += 'Type: '+this.type + '<br>';
					html += 'Prijs per dag: &euro;' + +parseFloat(this.prijs).toFixed(2);
					html += '</div>';
					html += '</div><br>';
				});
				$('.centered-box').append(html); 
			}else{
				$('.centered-box').append("<h3>Er zijn nog geen auto's beschikbaar, probeer het later nog eens.</h3>");
			}
		}
	});
	
	if(total_rows != null){
		$('#page-numbers').html('');
		var count = 1;
		for(var x = 0;  x < total_rows; x += rows_per_page)
		{
			if(count == current_page){
				$('#page-numbers').append('<li class=\"active\"><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
			}else{
				$('#page-numbers').append('<li><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
			}
			count++;
		}
	}
}

function huurAuto(){
	var vanaf = toDate($("#vanaf").val());
	var tot = toDate($("#tot").val());
	var datumerror = "";
	if(vanaf.getTime()+86400000 < new Date().getTime()){
		datumerror = "De 'vanaf' datum mag niet in het verleden zijn!";
	}else if(tot.getTime() < vanaf.getTime()){
		datumerror = "De datums kloppen niet.";
	}
	if(datumerror != ""){
		$("#autoPostForm").before("<div class=\"alert alert-warning\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>"+datumerror+"</div>");
		return;
	}
	
	if(auto_id != null){
		// zet de id's in de form
		$("#klantid").val(klant_id);
		$("#autoid").val(auto_id);
		
		$.ajax({
			url : "/restservices/autos/huur",
			type: "POST",
			contentType: "application/x-www-form-urlencoded",
			data : $("#autoPostForm").serialize(),
			success: function(data, textStatus, jqXHR)
			{	
				getPage(current_page);
				if(data.message == "false"){
					var message = "<div class=\"alert alert-warning\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>Er ging iets mis, is alles correct ingevuld?</div>";

					$("#autoPostForm").before(message);
				}else{
					hideModalWindow();
					$(".centered-box").prepend("<div class=\"alert alert-success\" style=\"width: 60%; margin: 0 auto;\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>Auto gereserveerd!</div><br>");
					$("#vanaf, #tot").val("");
				}
			},
			error: function (jqXHR, textStatus, errorThrown)
			{	
				$("#autoPostForm").before("<div class=\"alert alert-danger\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>Er ging iets mis, probeer het later aub opnieuw.</div>");
				$("#light").scrollTop(0);
			}
		});
	}
}

function disableDays(date){	
	date = jQuery.datepicker.formatDate('yy-mm-dd', date);
	console.log(date+"    "+blocked_days[0]);
	return [ blocked_days.indexOf(date) == -1 ];
}

function blockDays(){
	blocked_days = [];
	$.each(huur_datums, function(indx, elem){
		if(elem.autoid == auto_id){
			console.log(elem.autoid + "  " + auto_id);
			startdatum = toDate(formatDate(elem.startdatum));
			einddatum = toDate(formatDate(elem.einddatum));
			
			oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
			daysBetween = Math.round(Math.abs((startdatum.getTime() - einddatum.getTime())/(oneDay)));
			blocked_days.push(jQuery.datepicker.formatDate('yy-mm-dd', startdatum));
			while(daysBetween > 0){
				date = new Date(startdatum.setTime(startdatum.getTime() + 1 * 86400000 ));
				blocked_days.push(jQuery.datepicker.formatDate('yy-mm-dd', date));
				daysBetween--;
			}
		}
	});
}

function toDate(dateStr) {
    var parts = dateStr.split("-");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function formatDate(dateStr){
	var parts = dateStr.split("-");
	return $.datepicker.formatDate('dd-mm-yy', new Date(parts[0], parts[1]-1, parts[2]));
}

//kijkt welke "pagina" de user te zien moet krijgen
if(location.hash.slice(1) == ""){
	getPage(1);
	current_page = 1;
}else if(!isNaN(location.hash.slice(1))){
	getPage(location.hash.slice(1));
	current_page = location.hash.slice(1);
}
initPageNumbers();