var rows_per_page = 10; // hoeveel records er per keer in de table komen
var total_rows; // totaal aantal auto's in de database
var current_page = 1;
var auto_lijst = [];

// maakt de pagination
function initPageNumbers(){
	$('#page-numbers').html('');
	if(null == total_rows){
		$.get('/restservices/autos/total', function(data){
			total_rows = parseInt(data.total);
			
			if(total_rows < rows_per_page){
				return;
			}
			
			var count = 1;
			for(var x = 0;  x < total_rows; x += rows_per_page)
			{
				if(count == current_page){
					$('#page-numbers').append('<li class=\"active\"><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
				}else{
					$('#page-numbers').append('<li><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
				}
				count++;
			}
		});
	}else{
		var count = 1;
		for(var x = 0;  x < total_rows; x += rows_per_page)
		{
			if(count == current_page){
				$('#page-numbers').append('<li class=\"active\"><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
			}else{
				$('#page-numbers').append('<li><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
			}
			count++;
		}
	}
}

function getPage(page_num){
	auto_lijst = [];
	current_page = page_num;
	page_num = (page_num-1)*rows_per_page;
	$('#autotable').html('');
	
	$.get('/restservices/autos/all/'+page_num+'/'+rows_per_page, function(data){
		if(data.length > 0){
			html = '';
			html += '<thead><tr><th>Merk</th>';
			html += '<th>Model</th>';
			html += '<th>Type</th>';
			html += '<th>Prijs</th>';
			html += '<th>Brandstof</th>';
			html += '<th>Beschikbaar</th>';
			html += '</tr></thead>';
			html += '<tbody>';
			$(data).each(function(){
				html += '<tr>';
				html += '<td>'+this.merk+'</td>';
				html += '<td>'+this.model+'</td>';
				html += '<td>'+this.type+'</td>';
				html += '<td>&euro;  '+parseFloat(this.prijs).toFixed(2)+'</td>';
				html += '<td>'+this.brandstof+' L</td>';
				html += '<td>'+(this.verhuurbaar ? 'Ja' : 'Nee');
				html += '<a href="#" class="close" onclick="deleteAuto('+this.id+')">&times</a>';
				html += '<a href="#" class="pencil" onclick="showUpdateWindow('+this.id+')"><span class="glyphicon glyphicon-pencil" aria-hidden="true">';
				html += '</td>';
				html += '</tr>';
				
				auto = {id: this.id, merk: this.merk, model: this.model, type: this.type, prijs: this.prijs, brandstof: this.brandstof, verhuurbaar: this.verhuurbaar};
				auto_lijst.push(auto);
			});
			html += '</tbody>';
			$('#autotable').html(html); 
		}else{
			$('#autotable').html("<h3>Er zijn nog geen auto's toegevoegd.</h3>");
		}
	});		

	if(total_rows != null){
		$('#page-numbers').html('');
		var count = 1;
		for(var x = 0;  x < total_rows; x += rows_per_page)
		{
			if(count == current_page){
				$('#page-numbers').append('<li class=\"active\"><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
			}else{
				$('#page-numbers').append('<li><a href="#'+count+'" onclick="getPage('+count+');">'+count+'</a></li>');
			}
			count++;
		}
	}
}

function autoToevoegen(){
	if($("brandstof").val() == ""){
		$("brandstof").val("a");
	}
	$.ajax({
		url : "/restservices/autos/new",
		type: "POST",
		contentType: "application/x-www-form-urlencoded",
		data : $("#autoPostForm").serialize(),
		success: function(data, textStatus, jqXHR)
		{	
			if(data.errors.length > 0){
				var message = "<div class=\"alert alert-warning\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a><strong>Er zijn wat fouten opgetreden:</strong><br><ul>";

				for(var i =0; i < data.errors.length; i++){
					message = message + "<li>"+data.errors[i]+"</li>";
				}
				message = message + "</ul></div>"

				$("#autoPostForm").before(message);
			}else{
				$("#autotable").before("<div class=\"alert alert-success\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>Auto succesvol toegevoegd!</div>");
				hideModalWindow();
			}
			getPage(current_page);
			total_rows+=1;
			initPageNumbers();
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			$("#autoPostForm").before("<div class=\"alert alert-danger\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>Er ging iets mis, zijn alle velden correct ingevuld?</div>");
			$("#light").scrollTop(0);
		}
	});
}

function showUpdateWindow(id){
	var auto;
	$(auto_lijst).each(function(){
		if(this.id == id){
			auto = this;
			return false;
		}
	});
	
	$("#autoPostForm fieldset input[id=id]").val(auto.id);
	$("#autoPostForm fieldset input[id=merk]").val(auto.merk);
	$("#autoPostForm fieldset input[id=model]").val(auto.model);
	$("#autoPostForm fieldset input[id=type]").val(auto.type);
	$("#autoPostForm fieldset input[id=prijs]").val(parseFloat(auto.prijs).toFixed(2));
	$("#autoPostForm fieldset input[id=brandstof]").val(auto.brandstof);
	$("#autoPostForm fieldset select[id=verhuurbaar]").val(auto.verhuurbaar ? "true" : "false");
	
	$("#autoPostForm div button[id=addauto]").attr('onclick', 'updateAuto('+auto.id+')');
	$("#autoPostForm div button[id=canceladd]").attr('onclick', 'resetModalWindow()');
	$("#autoPostForm div button[id=addauto]").html("Aanpassen");
	
	showModalWindow();
}

function resetModalWindow(){
	$("#autoPostForm fieldset input[id=id]").val();
	$("#autoPostForm fieldset input[id=merk]").val("");
	$("#autoPostForm fieldset input[id=model]").val("");
	$("#autoPostForm fieldset input[id=type]").val("");
	$("#autoPostForm fieldset input[id=prijs]").val("");
	$("#autoPostForm fieldset input[id=brandstof]").val("");
	$("#autoPostForm fieldset select[id=verhuurbaar]").val("true");
	
	$("#autoPostForm div button[id=addauto]").attr('onclick', 'autoToevoegen()');
	$("#autoPostForm div button[id=canceladd]").attr('onclick', 'hideModalWindow()');
	$("#autoPostForm div button[id=addauto]").html("Toevoegen");
	hideModalWindow();
}

function updateAuto(id){
	$.ajax({
		url : "/restservices/autos/update",
		type: "POST",
		contentType: "application/x-www-form-urlencoded",
		data : $("#autoPostForm").serialize(),
		success: function(data, textStatus, jqXHR)
		{	
			if(data.errors.length > 0){
				var message = "<div class=\"alert alert-warning\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a><strong>Er zijn wat fouten opgetreden:</strong><br><ul>";

				for(var i =0; i < data.errors.length; i++){
					message = message + "<li>"+data.errors[i]+"</li>";
				}
				message = message + "</ul></div>";

				$("#autoPostForm").before(message);
			}else{
				$("#autotable").before("<div class=\"alert alert-success\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>Auto succesvol aangepast!</div>");
				hideModalWindow();
				resetModalWindow();
			}
			getPage(current_page);
		},
		error: function (jqXHR, textStatus, errorThrown)
		{	
			$("#autoPostForm").before("<div class=\"alert alert-danger\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>Er ging iets mis, zijn alle velden correct ingevuld?</div>");
			$("#light").scrollTop(0);
		}
	});
}

function deleteAuto(id){
	if (confirm('Weet je zeker dat je deze auto wilt verwijderen?')) {
		$.ajax({
			url : "/restservices/autos/delete/"+id,
			type: "GET",
			success: function(data, textStatus, jqXHR)
			{	
				$("#autotable").before("<div class=\"alert alert-success\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>Auto verwijderd!</div>");
				getPage(current_page);
				total_rows-=1;
				initPageNumbers();
			},
			error: function (jqXHR, textStatus, errorThrown)
			{	
				$("#autotable").before("<div class=\"alert alert-danger\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>Er ging iets mis, probeer het later opnieuw.</div>");
			}
		});
	}
}

function numberOnly(event){
	if(event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46 || event.charCode == 0){
		return true;
	}else{
		event.preventDefault();
	}
}

// kijkt welke "pagina" de user te zien moet krijgen #1 = pagina 1
if(location.hash.slice(1) == ""){
	getPage(1);
	current_page = 1;
}else if(!isNaN(location.hash.slice(1))){
	getPage(location.hash.slice(1));
	current_page = location.hash.slice(1);
}
initPageNumbers();
