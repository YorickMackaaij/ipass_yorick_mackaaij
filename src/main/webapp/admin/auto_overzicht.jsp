<jsp:include page="/header.jsp" />
<div id="light" class="white_content">
	<div class="text-left">
		<form method="POST" enctype="application/x-www-form-urlencoded" action="/admin/auto_overzicht.do" id="autoPostForm">
			<fieldset class="form-group"> 
				<input type="hidden" class="form-control" name="id" id="id">
			</fieldset>
			<fieldset class="form-group">
				<label for="merk">Merk</label> 
				<input type="text" maxlength="50" class="form-control" name="merk" id="merk" placeholder="Merk van de auto">
			</fieldset>
			<fieldset class="form-group">
				<label for="model">Model</label> 
				<input type="text" maxlength="50" class="form-control" name="model" id="model" placeholder="Model van de auto">
			</fieldset>
			<fieldset class="form-group">
				<label for="type">Type</label> 
				<input type="text" maxlength="50" class="form-control" name="type" id="type" placeholder="Type van de auto">
			</fieldset>
			<fieldset class="form-group">
				<label for="prijs">Prijs</label> 
				<input type="number" step="0.01" min="0" name="prijs" id="prijs" class="form-control" onkeypress='numberOnly(event);' placeholder="Prijs per dag in euro's">
			</fieldset>
			<fieldset class="form-group">
				<label for="brandstof">Brandstof</label> 
				<input type="number" max="200" min="0" name="brandstof" id="brandstof" class="form-control" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 0' placeholder="Hoeveel liter brandstof zit er in de tank?">
			</fieldset>
			<fieldset class="form-group">
				<label for="brandstof">Beschikbaar voor klanten</label><br>
				<select id="verhuurbaar" name="verhuurbaar">
					<option value="true">Ja</option>
					<option value="false">Nee</option>
				</select> 
			</fieldset>
			<hr>
			<div class="text-center">
				<button type="button" id="addauto" class="btn btn-primary" onclick="autoToevoegen()">Toevoegen</button>
				<button type="button" id="canceladd" class="btn btn-secondary" onclick="hideModalWindow()">Annuleren</button>
			</div>
			<br>
		</form>
	</div>
</div>
<div id="fade" class="black_overlay"></div>
<div class="centered-box">
	<button type="button" class="btn btn-primary" onclick="showModalWindow()">Nieuwe auto toevoegen</button>
	<br><br>
	<div class="table-responsive">
		<table id="autotable" class="table"></table>
	</div>
	<div class="text-center">
		<ul class="pagination" id="page-numbers"></ul>
	</div>
</div>
<script src="/assets/js/modal_window.js"></script>
<script src="/assets/js/admin_ajax.js"></script>
<jsp:include page="/footer.jsp" />