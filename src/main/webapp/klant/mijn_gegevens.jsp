<jsp:include page="/header.jsp" />
<div class="panel panel-default" style="width: 50%;">
	<dl class="dl-horizontal">
		<h3 class="text-center">Mijn Gegevens</h3>
		<hr>
		<dt>Naam:</dt>
		<dd>${klant.voornaam}</dd>
		<dt>Tussenvoegsels:</dt>
		<dd>${klant.tussenvoegsels}</dd>
		<dt>Achternaam:</dt>
		<dd>${klant.achternaam}</dd>
		<dt>Adres:</dt>
		<dd>${klant.adres}</dd>
		<dt>Postcode:</dt>
		<dd>${klant.postcode}</dd>
	</dl>
</div>
<jsp:include page="/footer.jsp" />