<jsp:include page="/header.jsp" />
<div id="light" class="white_content">
	<div class="text-left">
		<form method="POST" enctype="application/x-www-form-urlencoded" action="/admin/auto_overzicht.do" id="autoPostForm">
			<fieldset class="form-group"> 
				<input type="hidden" class="form-control" name="klantid" id="klantid">
				<input type="hidden" class="form-control" name="autoid" id="autoid">
			</fieldset>
			<fieldset class="form-group">
				<label for="merk">Huren vanaf</label> 
				<input type="text" class="form-control" name="vanaf" id="vanaf" placeholder="Geef een datum op">
			</fieldset>
			<fieldset class="form-group">
				<label for="merk">Huren t/m</label> 
				<input type="text" class="form-control" name="tot" id="tot" placeholder="Geef een datum op">
			</fieldset>
			<hr>
			<div class="text-center">
				<button type="button" id="addauto" class="btn btn-primary" onclick="huurAuto()">Huren</button>
				<button type="button" id="canceladd" class="btn btn-secondary" onclick="hideModalWindow()">Annuleren</button>
			</div>
			<br>
		</form>
	</div>
</div>
<div id="fade" class="black_overlay"></div>
<div class="centered-box"></div>
<div class="text-center">
	<ul class="pagination" id="page-numbers"></ul>
</div>
<script src="/assets/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script src="/assets/js/klant_ajax.js"></script>
<script src="/assets/js/modal_window.js"></script>
<jsp:include page="/footer.jsp" />